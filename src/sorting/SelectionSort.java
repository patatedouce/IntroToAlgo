package sorting;

public class SelectionSort implements Sort{
    @Override
    public int[] sort(int[] a, StepAction sa){
	for(int i=0; i<a.length; i++){
	    ArrayUtilities.swap(a, i, ArrayUtilities.min(a, i, a.length));
	    sa.doAction();
	}
	return a;
    }

    @Override
    public String toString(){
	return "Selection sort";
    }
}
