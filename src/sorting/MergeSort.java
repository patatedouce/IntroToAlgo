package sorting;

public class MergeSort implements Sort{
    private StepAction sa;
    
    @Override
    public int[] sort(int[] a, StepAction sa){
	this.sa = sa;
	return mergeSort(a, 0, a.length-1);
    }
    
    public int[] merge(int[] a, int p, int q, int r){
	int n1 = q - p + 1;
	int n2 = r - q;
	int[] left = new int[n1+1];
	int[] right = new int[n2+1];
	int i, j;
	for(i=0; i<n1; i++){
	    left[i] = a[p+i];
	}
	for(j=0; j<n2; j++){
	    right[j] = a[q+j+1];
	}
	left[n1] = right[n2] = Integer.MAX_VALUE;
	i = j = 0;
	for(int k=p; k<=r; k++){
	    if(left[i] < right[j]){
		a[k] = left[i++];
	    }else{
		a[k] = right[j++];
	    }
	}
	sa.doAction();
	return a;
    }

    public int[] mergeSort(int[] a, int p, int r){
	if(r > p){
	    int q = (p+r)/2;
	    mergeSort(a, p, q);
	    mergeSort(a, q+1, r);
	    merge(a, p, q, r);
	}
	return a;
    }

    @Override
    public String toString(){
	return "Merge sort";
    }
}
