package view;

import javax.swing.JPanel;
import javax.swing.ButtonGroup;
import javax.swing.AbstractButton;

import java.util.Enumeration;

import java.awt.Graphics;

import sorting.SortFactory;
import sorting.Sort;


public class SortView extends JPanel{
    private ButtonGroup bg;
    private SortOptions so;

    public SortView(){
	this.bg = new ButtonGroup();
	SortFactory sf = new SortFactory();
        sf.getAllSort().forEach(sort -> {
		SortButton sb = new SortButton(sort);
		bg.add(sb);
		this.add(sb);
		sb.addActionListener(e -> displaySortOptions(sort));
	    });
    }

    public void displaySortView(){
        Enumeration<AbstractButton> buttons = bg.getElements();
	while(buttons.hasMoreElements()){
	    buttons.nextElement().setVisible(true);
	}
	this.remove(so);
    }
    
    public void displaySortOptions(Sort sort){
        Enumeration<AbstractButton> buttons = bg.getElements();
	while(buttons.hasMoreElements()){
	    buttons.nextElement().setVisible(false);
	}
	so = new SortOptions(sort);
	this.getParent().add(so);
    }

    @Override
    public void paintComponent(Graphics g){
	super.paintComponent(g);
	if(so != null){
	    so.setSize(this.getWidth(), this.getHeight());
	}
    }
}
