package view;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.Graphics;

public class View extends JFrame{
    private JButton sortButton;
    private SortView sv;

    public View(){
	setSize(800,600);
	setVisible(true);
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	sortButton = new JButton("Sort");
	sortButton.addActionListener(e -> displaySortView());
	this.add(sortButton);
	
	sv = new SortView();
    }

    public void displaySortView(){
	sortButton.setVisible(false);
	this.add(sv);
    }

    @Override
    public void paintComponents(Graphics g){
	sv.setSize(this.getWidth(), this.getHeight());
    }
	
}
