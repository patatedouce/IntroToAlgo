package view;

import javax.swing.JButton;

import sorting.Sort;

public class SortButton extends JButton{
    private Sort sort;

    public SortButton(Sort s){
	this.sort = s;
	this.setText(s.toString());
    }

    public Sort getSort(){
	return sort;
    }
}
